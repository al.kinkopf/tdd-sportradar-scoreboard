# TDD-sportradar-scoreboard

## Short about

First things first. Pardon the grammar. I wrote this during half time break real madrid - bayern.
So...where to start? With the entities of course. I always start with entities. What do we have? A team, a match and an event.
The project is divided into BusinessLogic (services) and Models. Just this...and unit tests of course.

## Entities and some business logic

### TEAM
A team can be male, female, U18, U21 and so on. But let's keep it simple. Let just have nationalities.

### MATCH
Ok...here we can start to complicate things. But still..let's keep it simple. A match will have 4 statuses: registered, started, finished and failed registration (or something like that). A match to be registered must follow some simple rules. 2 different teams and null is not acceptable. There can be multiple registered matches with same teams but just 1 can be "started" in a given moment. Ahh of course we have start datetime because we have to order at the end using this atribute. 
Start datetime atribute is optional. If not given is datetime.now else you can pass this atribute (why not). So of course there are some other business rules about matches...

### EVENT
We have event goal. Just goal. Goal = team score ++.
When I started I was thinking about events like goal, forfeit, goal cancelled and so on... I keept it simple. Here we also have some business rules. Only started match can have an event goal for example.

### Scoreboard
This is an entity that is actually a list of matches. All the actions/events are actually reflected in the scoreboard.
Of course also here we have some business rules. But actually this part was easy. I lost the majority of the time with matches/events combinations and business rules.
Maybe I didn't keep it so simple but still I could complicate everything a lot more.

Go to go...match is on :) 

## PS

Ugh..reading again this: "Finish match currently in progress. This removes a match from the scoreboard."
Actually I implemented a method that is returning all matches and a method that gets status as parameter so it can return live or finished matches.
I know that is not by requirment but from the begining I was thinking about having a "complete" scoreboard with all live and already finished matches.