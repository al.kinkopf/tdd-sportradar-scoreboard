﻿using FluentAssertions;
using FluentAssertions.Extensions;
using TDDSportradarScoreboard.BL.MatchBL;
using TDDSportradarScoreboard.Model;
using TDDSportradarScoreboard.Model.Enum;

namespace TDDSportradarScoreboard.Tests
{
    public class MatchServiceTests
    {
        #region teams
        static Team Uruguay = new(Nationality.Uruguay);
        static Team Italy = new(Nationality.Italy);
        static Team Spain = new(Nationality.Spain);
        static Team Brazil = new(Nationality.Brazil);
        static Team Mexico = new(Nationality.Mexico);
        static Team Canada = new(Nationality.Canada);
        static Team Argentina = new(Nationality.Argentina);
        static Team Australia = new(Nationality.Australia);
        static Team Germany = new(Nationality.Germany);
        static Team France = new(Nationality.France);
        static Team Serbia = new(Nationality.Serbia);
        static Team Slovenia = new(Nationality.Slovenia);
        static Team Japan = new(Nationality.Japan);
        static Team Croatia = new(Nationality.Croatia);
        #endregion

        #region TEST: Chek match constructor
        public static IEnumerable<object[]> TeamMatchList => new List<object[]>
        {
            new object[] { Uruguay, Italy, MatchStatus.Registered, 0 },
            new object[] { Brazil, Brazil, MatchStatus.RegistrationFailed, 0 }, // 2x same nationality
            new object[] { Slovenia, null, MatchStatus.RegistrationFailed, 0 }, // home team null nationality
            new object[] { null, Slovenia, MatchStatus.RegistrationFailed, 0 }, // away team null nationality
            new object[] { Uruguay, Italy, MatchStatus.Registered, 0}
        };

        [Theory, MemberData(nameof(TeamMatchList))]
        public void Match_Constructor_ReturnObject_Ok(Team homeTeam, Team awayTeam, MatchStatus matchStatus, int id)
        {
            //Arange

            //Act
            var result = new Match(homeTeam, awayTeam);

            //Assert
            result.Should().BeOfType<Match>();
            result.MatchStatus.Should().Be(matchStatus);
            result.HomeTeam.Should().Be(homeTeam);
            result.AwayTeam.Should().Be(awayTeam);
            result.Id.Should().BeGreaterThan(id);

        }
        #endregion

        #region TEST: Chek if match parameters are valid for start

        public static IEnumerable<object[]> MatchValidList => new List<object[]>
        {
            new object[] { Canada, Spain, false, false, true }, // register match --> valid for start
            new object[] { Brazil, Mexico, true, true, false }, // register, start, finish match --> not valid for start
            new object[] { France, Germany, true, false, false }, // register, start match --> not valid for start
            new object[] { Uruguay, Spain, true, false, false }, //Home team already playing
            new object[] { Spain, Uruguay, true, false, false }, // Away team already playing
            new object[] { Slovenia, null, false, false, false }, // 1 team null not valid
            new object[] { null, Slovenia, false, false, false }, // 1 team null not valid
            new object[] { Slovenia, Slovenia, false, false, false }, // 2x same team not valid
            new object[] { Slovenia, null, true, false, false }, // 1 team null not valid
            new object[] { null, Slovenia, true, false, false }, // 1 team null not valid
            new object[] { Slovenia, Slovenia, true, false, false }, // 2x same team not valid
            new object[] { Slovenia, null, true, true, false }, // 1 team null not valid
            new object[] { null, Slovenia, true, true, false }, // 1 team null not valid
            new object[] { Slovenia, Slovenia, true, true, false }, // 2x same team not valid
        };

        [Theory, MemberData(nameof(MatchValidList))]
        public void MatchService_IsMatchStartValid_ReturnBool(Team homeTeam, Team awayTeam, bool startMatch, bool endMatch, bool expected)
        {
            //Arange
            ScoreBoard scoreBoard = new();
            var match = new Match(homeTeam, awayTeam);

            if (startMatch)
            {
                MatchService.StartMatch(match, scoreBoard);
            }

            if (endMatch)
            {
                MatchService.FinishMatch(match);
            }

            //Act
            var result = MatchService.IsMatchStartValid(match, scoreBoard);

            //Assert
            result.Should().Be(expected);
        }
        #endregion

        #region  TEST: Chek start match method + match object updated ok + scoreboard updated ok

        static DateTime startGameDateTime = new DateTime(2024, 05, 21, 11, 00, 00);

        public static IEnumerable<object[]> MatchStartList => new List<object[]>
        {
            new object[] { Canada, Spain, false, false, true, null }, // test start match and start date teime
            new object[] { Brazil, Mexico,  false, false, true, startGameDateTime }, // test start match and start date teime
            new object[] { France, Germany, false, true, true, null }, // try to finish not started match
            new object[] { Uruguay, Spain, true, false, false, null }, //  match already started
            new object[] { Japan, Serbia, true, true, false, null }, // match started and already finised
            new object[] { Slovenia, null, false, false, false, null }, // 1 team null not valid
            new object[] { null, Slovenia, false, false, false, null }, // 1 team null not valid
            new object[] { Slovenia, Slovenia, false, false, false, null }, // 2x same team not valid
            new object[] { Slovenia, null, true, false, false, null }, // 1 team null not valid
            new object[] { null, Slovenia, true, false, false, null }, // 1 team null not valid
            new object[] { Slovenia, Slovenia, true, false, false, null }, // 2x same team not valid
            new object[] { Slovenia, null, true, true, false, null }, // 1 team null not valid
            new object[] { null, Slovenia, true, true, false, null }, // 1 team null not valid
            new object[] { Slovenia, Slovenia, true, true, false, null }, // 2x same team not valid
        };

        [Theory, MemberData(nameof(MatchStartList))]
        public void MatchService_StartMatch_ChangedObject(Team homeTeam, Team awayTeam, bool startMatch, bool endMatch, bool expected, DateTime? startDate = null)
        {
            //Arange
            ScoreBoard scoreBoard = new();
            var dateTimeNow = DateTime.Now;

            var match = new Match(homeTeam, awayTeam);

            if (startMatch)
            {
                MatchService.StartMatch(match, scoreBoard, startDate);
            }

            if (endMatch)
            {
                MatchService.FinishMatch(match);
            }

            //Act          
            var result = MatchService.StartMatch(match, scoreBoard, startDate);

            //Assert
            result.Should().Be(expected);


            if (expected)
            {
                match.MatchStatus.Should().Be(MatchStatus.Started);
                match.HomeTeam.Should().NotBeNull();
                match.AwayTeam.Should().NotBeNull();
                match.HomeTeamScore.Should().Be(0);
                match.AwayTeamScore.Should().Be(0);
                match.AwayTeam.Nationality.Should().Be(awayTeam.Nationality);
                match.HomeTeam.Nationality.Should().Be(homeTeam.Nationality);
                scoreBoard.Should().NotBeNull();
                scoreBoard.MatchList.Should().NotBeNull();

                if (startDate != null)
                {
                    match.StartDate.Should().Be(startDate);
                }
                else
                {
                    match.StartDate.Should().BeCloseTo(dateTimeNow, 2.Seconds());
                }
            }
        }
        #endregion

        #region TEST: Check finish match method + match object updated + scoreboard updated ok
        static DateTime endGameDateTime = new DateTime(2024, 05, 21, 11, 00, 00);

        public static IEnumerable<object[]> MatchFinishList => new List<object[]>
        {
            new object[] { Canada, Spain, true, false, true, null }, // test that end time will be date time now
            new object[] { Brazil, Mexico,  true, false, true, endGameDateTime }, // test that end time will be from parameter
            new object[] { France, Germany, false, false, false, null }, // match never started
            new object[] { Uruguay, Spain, false, true, false, null }, //  match never started
            new object[] { Japan, Serbia, true, true, false, null }, // match started and already finised
            new object[] { Slovenia, null, false, false, false, null }, // 1 team null not valid
            new object[] { null, Slovenia, false, false, false, null }, // 1 team null not valid
            new object[] { Slovenia, Slovenia, false, false, false, null }, // 2x same team not valid
            new object[] { Slovenia, null, true, false, false, null }, // 1 team null not valid
            new object[] { null, Slovenia, true, false, false, null }, // 1 team null not valid
            new object[] { Slovenia, Slovenia, true, false, false, null }, // 2x same team not valid
            new object[] { Slovenia, null, true, true, false, null }, // 1 team null not valid
            new object[] { null, Slovenia, true, true, false, null }, // 1 team null not valid
            new object[] { Slovenia, Slovenia, true, true, false, null }, // 2x same team not valid
        };

        [Theory, MemberData(nameof(MatchFinishList))]
        public void MatchService_FinishMatch_ReturnBool(Team homeTeam, Team awayTeam, bool startMatch, bool endMatch, bool expected, DateTime? endDate = null)
        {
            //Arange
            ScoreBoard scoreBoard = new();
            var dateTimeNow = DateTime.Now;

            var match = new Match(homeTeam, awayTeam);

            if (startMatch)
            {
                MatchService.StartMatch(match, scoreBoard);
            }

            if (endMatch)
            {
                MatchService.FinishMatch(match);
            }

            //Act          
            var result = MatchService.FinishMatch(match, endDate);

            //Assert
            result.Should().Be(expected);

            if (expected)
            {
                match.MatchStatus.Should().Be(MatchStatus.Finished);
                if (endDate != null)
                {
                    match.EndDate.Should().Be(endDate);
                }
                else
                {
                    match.EndDate.Should().BeCloseTo(dateTimeNow, 2.Seconds());
                }
            }
        }
        #endregion
    }
}
