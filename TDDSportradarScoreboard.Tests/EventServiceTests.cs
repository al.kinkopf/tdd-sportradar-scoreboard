﻿using FluentAssertions;
using TDDSportradarScoreboard.BL.EventBL;
using TDDSportradarScoreboard.BL.MatchBL;
using TDDSportradarScoreboard.Model;
using TDDSportradarScoreboard.Model.Enum;

namespace TDDSportradarScoreboard.Tests
{
    public class EventServiceTests
    {
        #region teams, matches and events
        static Team Uruguay = new(Nationality.Uruguay);
        static Team Italy = new(Nationality.Italy);
        static Team Spain = new(Nationality.Spain);
        static Team Brazil = new(Nationality.Brazil);
        static Team Mexico = new(Nationality.Mexico);
        static Team Canada = new(Nationality.Canada);
        static Team Argentina = new(Nationality.Argentina);
        static Team Australia = new(Nationality.Australia);
        static Team Germany = new(Nationality.Germany);
        static Team France = new(Nationality.France);
        static Team Serbia = new(Nationality.Serbia);
        static Team Slovenia = new(Nationality.Slovenia);
        static Team Japan = new(Nationality.Japan);
        static Team Croatia = new(Nationality.Croatia);

        static CreateEventInput homeTeamScore = new(EventType.Goal, null, true);
        static CreateEventInput awayTeamScore = new(EventType.Goal, null, false);

        static CreateEventInput homeTeamForfeit = new(EventType.Forfeit, null, true);
        static CreateEventInput awayTeamForfeit = new(EventType.Forfeit, null, false);
        #endregion

        #region TEST: Check if event is valid for amtch status

        public static IEnumerable<object[]> EventValidList => new List<object[]>
        {
            new object[] { Canada, Spain, EventType.Goal, false, false, false }, // not started match
            new object[] { Brazil, Mexico, EventType.Goal, true, true, false }, // already finished match
            new object[] { France, Germany, EventType.Goal, true, false, true }, // started matched
            new object[] { Uruguay, Spain, EventType.Goal, false, true , false }, // not started match
            new object[] { Slovenia, null, EventType.Goal, false, false, false }, // 1 team null not valid
            new object[] { null, Slovenia, EventType.Goal, false, false, false }, // 1 team null not valid
            new object[] { Slovenia, Slovenia, EventType.Goal, false, false, false }, // 2x same team not valid
            new object[] { Slovenia, null, EventType.Goal, true, false, false }, // 1 team null not valid
            new object[] { null, Slovenia, EventType.Goal, true, false, false }, // 1 team null not valid
            new object[] { Slovenia, Slovenia, EventType.Goal, true, false, false }, // 2x same team not valid
            new object[] { Slovenia, null, EventType.Goal, true, true, false }, // 1 team null not valid
            new object[] { null, Slovenia, EventType.Goal, true, true, false }, // 1 team null not valid
            new object[] { Slovenia, Slovenia, EventType.Goal, true, true, false }, // 2x same team not valid
        };

        [Theory, MemberData(nameof(EventValidList))]
        public void EventService_IsEventValid_ReturnBool(Team homeTeam, Team awayTeam, EventType eventType, bool startMatch, bool endMatch, bool expected)
        {
            //Arange
            ScoreBoard scoreBoard = new();

            var match = new Match(homeTeam, awayTeam);

            if (startMatch)
            {
                MatchService.StartMatch(match, scoreBoard);
            }

            if (endMatch)
            {
                MatchService.FinishMatch(match);
            }

            //Act
            var result = EventService.IsEventValid(match, eventType);

            //Assert
            result.Should().Be(expected);
        }
        #endregion



        public static IEnumerable<object[]> CreateEventList => new List<object[]>
        {
           new object[] { France, Germany, homeTeamScore, 1, 0, true },
           new object[] { Spain, Uruguay, awayTeamScore, 0, 1, true },
           new object[] { Canada, Spain, homeTeamForfeit, 0, 0, false },
           new object[] { Serbia, Slovenia, awayTeamForfeit, 0, 0, false },
           new object[] { null, Japan, homeTeamScore, 1, 0, false },
           new object[] { Japan, null, homeTeamScore, 1, 0, false },
           new object[] { Japan, Japan, awayTeamScore, 0, 1, false },
           new object[] { null, Japan, homeTeamForfeit, 0, 0, false },
           new object[] { Japan, null, homeTeamForfeit, 0, 0, false },
           new object[] { Japan, Japan, awayTeamForfeit, 0, 0, false }
        };

        [Theory, MemberData(nameof(CreateEventList))]
        public void EventService_CreateEvent_ReturnBool(Team homeTeam, Team awayTeam, CreateEventInput input, int homeTeamScore, int awayTeamScore, bool expected)
        {
            //Arange
            ScoreBoard scoreBoard = new();

            var match = new Match(homeTeam, awayTeam);

            MatchService.StartMatch(match, scoreBoard);

            //Act
            var result = EventService.CheckAndCreateEvent(match, input);

            result.Should().Be(expected);
            if (expected)
            {
                scoreBoard.MatchList.First().HomeTeamScore.Should().Be(homeTeamScore);
                scoreBoard.MatchList.First().AwayTeamScore.Should().Be(awayTeamScore);
            }
        }
    }
}
