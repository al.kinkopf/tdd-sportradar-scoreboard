﻿using TDDSportradarScoreboard.Model.Enum;
using TDDSportradarScoreboard.Model;
using TDDSportradarScoreboard.BL.MatchBL;
using TDDSportradarScoreboard.BL.EventBL;
using TDDSportradarScoreboard.BL.ScoreboardBL;
using FluentAssertions;

namespace TDDSportradarScoreboard.Tests
{
    public class ScoreboardServiceTests
    {
        #region teams, matches, events, scoreboard
        static Team Uruguay = new(Nationality.Uruguay);
        static Team Italy = new(Nationality.Italy);
        static Team Spain = new(Nationality.Spain);
        static Team Brazil = new(Nationality.Brazil);
        static Team Mexico = new(Nationality.Mexico);
        static Team Canada = new(Nationality.Canada);
        static Team Argentina = new(Nationality.Argentina);
        static Team Australia = new(Nationality.Australia);
        static Team Germany = new(Nationality.Germany);
        static Team France = new(Nationality.France);
        static Team Serbia = new(Nationality.Serbia);
        static Team Slovenia = new(Nationality.Slovenia);
        static Team Japan = new(Nationality.Japan);
        static Team Croatia = new(Nationality.Croatia);
        static Team England = new(Nationality.England);
        static Team Hungary = new(Nationality.Hungary);
        static Team Austria = new(Nationality.Austria);
        static Team China = new(Nationality.China);

        static Match matchUruguayItaly = new(Uruguay, Italy);
        static Match matchSpainBrazil = new(Spain, Brazil);
        static Match matchMexicoCanada = new(Mexico, Canada);
        static Match matchArgentinaAustralia = new(Argentina, Australia);
        static Match matchGermanyFrance = new(Germany, France);

        static ScoreBoard scoreBoardSportradarTest = new ScoreBoard();

        static CreateEventInput homeTeamScore = new(EventType.Goal, null, true);
        static CreateEventInput awayTeamScore = new(EventType.Goal, null, false);
        #endregion


        #region TEST entire workflow
        //Original challenge
        static List<Match> matchOrderList1 = new List<Match>() { matchMexicoCanada };
        static List<Match> matchOrderList2 = new List<Match>() { matchSpainBrazil, matchMexicoCanada };
        static List<Match> matchOrderList3 = new List<Match>() { matchSpainBrazil, matchMexicoCanada, matchGermanyFrance };
        static List<Match> matchOrderList4 = new List<Match>() { matchUruguayItaly, matchSpainBrazil, matchMexicoCanada, matchGermanyFrance };
        static List<Match> matchOrderList5 = new List<Match>() { matchUruguayItaly, matchSpainBrazil, matchMexicoCanada, matchArgentinaAustralia, matchGermanyFrance };

        public static IEnumerable<object[]> MatchListEvent => new List<object[]>
        {
            new object[] { matchMexicoCanada, 0, 5, 0, matchOrderList1, 5 },
            new object[] { matchSpainBrazil, 10, 2, 1, matchOrderList2, 17 },
            new object[] { matchGermanyFrance, 2, 2, 2, matchOrderList3, 21 },
            new object[] { matchUruguayItaly, 6, 6, 3, matchOrderList4, 33 },
            new object[] { matchArgentinaAustralia, 3, 1, 4, matchOrderList5, 37 },
        };


        //static List<Match> matchOrderList1 = new List<Match>() { matchMexicoCanada };
        //static List<Match> matchOrderList2 = new List<Match>() { matchSpainBrazil, matchMexicoCanada };
        //static List<Match> matchOrderList3 = new List<Match>() { matchGermanyFrance, matchSpainBrazil, matchMexicoCanada };
        //static List<Match> matchOrderList4 = new List<Match>() { matchUruguayItaly, matchGermanyFrance, matchSpainBrazil, matchMexicoCanada };
        //static List<Match> matchOrderList5 = new List<Match>() { matchArgentinaAustralia, matchUruguayItaly, matchGermanyFrance, matchSpainBrazil, matchMexicoCanada };

        //public static IEnumerable<object[]> MatchListEvent => new List<object[]>
        //{
        //    new object[] { matchMexicoCanada, 3, 3, 0, matchOrderList1, 6 },
        //    new object[] { matchSpainBrazil, 3, 3, 1, matchOrderList2, 12 },
        //    new object[] { matchGermanyFrance, 3, 3, 2, matchOrderList3, 18 },
        //    new object[] { matchUruguayItaly, 3, 3, 3, matchOrderList4, 24 },
        //    new object[] { matchArgentinaAustralia, 3, 3, 4, matchOrderList5, 30 },
        //};


        [Theory, MemberData(nameof(MatchListEvent))]
        public void ScoreboardService_GetScoreBoardLiveOrdered_ReturnList(Match match, int homeGoals, int awayGoals, int indexScoreboard, List<Match> matchOrderList, int sumAllGoals)
        {
            //Arange
            MatchService.StartMatch(match, scoreBoardSportradarTest);
            int originalHomeGoals = homeGoals;
            int originalAwayGoals = awayGoals;

            //Act
            while (homeGoals > 0)
            {
                EventService.CheckAndCreateEvent(match, homeTeamScore);
                homeGoals--;
            }

            while (awayGoals > 0)
            {
                EventService.CheckAndCreateEvent(match, awayTeamScore);
                awayGoals--;
            }

            var result = ScoreboardService.GetScoreBoardOrderedByStatus(scoreBoardSportradarTest, MatchStatus.Started);

            //Assert

            scoreBoardSportradarTest.MatchList.Count.Should().Be(matchOrderList.Count());

            int sumGoals = 0;
            foreach (var scoreBoardMatch in scoreBoardSportradarTest.MatchList)
            {
                sumGoals += scoreBoardMatch.AwayTeamScore + scoreBoardMatch.HomeTeamScore;
            }

            sumGoals.Should().Be(sumAllGoals);

            scoreBoardSportradarTest.MatchList[indexScoreboard].HomeTeamScore.Should().Be(originalHomeGoals);
            scoreBoardSportradarTest.MatchList[indexScoreboard].AwayTeamScore.Should().Be(originalAwayGoals);

            for (int i = 0; i < matchOrderList.Count; i++)
            {
                result.MatchList[i].HomeTeam.Should().Be(matchOrderList[i].HomeTeam);
                result.MatchList[i].AwayTeam.Should().Be(matchOrderList[i].AwayTeam);
            }
        }
        #endregion

        
        [Fact]
        public void ScoreboardService_IsScoreBoardValid_ReturnBool()
        {
            //Arange
            ScoreBoard scoreBoard = new();
            scoreBoard.MatchList = null;

            ScoreBoard scoreBoardMatchListCountZero = new();

            ScoreBoard scoreBoardMatch = new();
            Match matchSerbiaSlovenia = new(Serbia, Slovenia);
            MatchService.StartMatch(matchSerbiaSlovenia, scoreBoardMatch);

            //Act
            var result = ScoreboardService.IsScoreBoardValid(scoreBoard);
            var resultNull = ScoreboardService.IsScoreBoardValid(null);
            var resultCountZero = ScoreboardService.IsScoreBoardValid(scoreBoardMatchListCountZero);
            var resultMatch = ScoreboardService.IsScoreBoardValid(scoreBoardMatch);

            //Assert
            result.Should().Be(false);
            resultNull.Should().Be(false);
            resultCountZero.Should().Be(false);
            resultMatch.Should().Be(true);
        }

        //Testing returning order of all and finished matches
        [Fact]
        public void ScoreboardService_GetScoreBoard_ReturnList()
        {
            //Arange
            ScoreBoard scoreBoard = new();
            DateTime startGameDateTime1 = new DateTime(2021, 05, 21, 11, 00, 00);
            DateTime startGameDateTime2 = new DateTime(2022, 05, 21, 11, 00, 00);
            DateTime startGameDateTime3 = new DateTime(2023, 05, 21, 11, 00, 00);

            Match matchSerbiaSlovenia = new(Serbia, Slovenia);
            MatchService.StartMatch(matchSerbiaSlovenia, scoreBoard, startGameDateTime2);

            Match matchChinaAustria = new(China, Austria);
            MatchService.StartMatch(matchChinaAustria, scoreBoard, startGameDateTime3);
            MatchService.FinishMatch(matchChinaAustria);

            Match matchHungaryEngland = new(Hungary, England);
            MatchService.StartMatch(matchHungaryEngland, scoreBoard, startGameDateTime1);
            MatchService.FinishMatch(matchHungaryEngland);

            //Added 2 invalid matches to see the behaviour 10/05/2024
            Match matchJapanNull = new(Japan, null); //null team --> invalid
            MatchService.StartMatch(matchJapanNull, scoreBoard, startGameDateTime2);

            Match matchJapanSlovenia = new(Japan, Slovenia); // Slovenia already playing
            MatchService.StartMatch(matchJapanSlovenia, scoreBoard, startGameDateTime1);

            //Act
            var resultAll = ScoreboardService.GetScoreBoardOrderedAll(scoreBoard);
            var resultFinished = ScoreboardService.GetScoreBoardOrderedByStatus(scoreBoard, MatchStatus.Finished);

            //Assert
            resultAll.MatchList.Should().HaveCount(3);
            resultAll.MatchList[0].HomeTeam.Should().Be(China);
            resultAll.MatchList[0].AwayTeam.Should().Be(Austria);

            resultAll.MatchList[1].HomeTeam.Should().Be(Serbia);
            resultAll.MatchList[1].AwayTeam.Should().Be(Slovenia);

            resultAll.MatchList[2].HomeTeam.Should().Be(Hungary);
            resultAll.MatchList[2].AwayTeam.Should().Be(England);

            resultFinished.MatchList.Should().HaveCount(2);
            resultFinished.MatchList[0].HomeTeam.Should().Be(China);
            resultFinished.MatchList[0].AwayTeam.Should().Be(Austria);

            resultFinished.MatchList[1].HomeTeam.Should().Be(Hungary);
            resultFinished.MatchList[1].AwayTeam.Should().Be(England);
        }
    }
}
