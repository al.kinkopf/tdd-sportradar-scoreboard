﻿using TDDSportradarScoreboard.Model.Enum;
using System.Diagnostics.CodeAnalysis;

namespace TDDSportradarScoreboard.Model
{
    public class Match
    {
        static int numberOfInstances = 1;

        [method: SetsRequiredMembers]
        public Match()
        {
        }

        [method: SetsRequiredMembers]
        public Match(Team homeTeam, Team awayTeam) {

            HomeTeam = homeTeam;
            AwayTeam = awayTeam;

            Id = numberOfInstances;
            numberOfInstances++;

            EventList = [];

            if ((homeTeam != null && awayTeam != null) && (homeTeam != awayTeam))
            {

                MatchStatus = MatchStatus.Registered;
            }
            else
            {
                MatchStatus = MatchStatus.RegistrationFailed;
            }
        }

        public int Id { get; set; }
        public int HomeTeamScore { get; set; }
        public int AwayTeamScore { get; set; }
        public required Team HomeTeam { get; set; }
        public required Team AwayTeam { get; set; }
        public MatchStatus MatchStatus { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<Event> EventList { get; set; }
    }

    public class ScoreBoard
    {
       public List<Match>? MatchList { get; set; }
    }
}
