﻿using TDDSportradarScoreboard.Model.Enum;
using System.Diagnostics.CodeAnalysis;

namespace TDDSportradarScoreboard.Model
{
    [method: SetsRequiredMembers]
    public class Team(Nationality nationality)
    {
        public required Nationality Nationality { get; set; } = nationality;
        public int Score { get; set; }
    }
}
