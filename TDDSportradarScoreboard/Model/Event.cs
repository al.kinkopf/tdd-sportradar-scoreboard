﻿using TDDSportradarScoreboard.Model.Enum;
using System.Diagnostics.CodeAnalysis;

namespace TDDSportradarScoreboard.Model
{
    public class Event
    {
        static int numberOfInstances = 1;

        [method: SetsRequiredMembers]
        public Event(EventType eventType, DateTime? eventDate = null)
        {
            Id = numberOfInstances;
            numberOfInstances++;

            EventType = eventType;

            if (eventDate == null)
            {
                EventDate = DateTime.Now;
            }
            else
            {
                EventDate = (DateTime)eventDate;
            }
        }

        public int Id { get; set; }
        public required EventType EventType { get; set; }
        public DateTime EventDate { get; set; }
    }

    public class CreateEventInput
    {
        [method: SetsRequiredMembers]
        public CreateEventInput(EventType eventType, DateTime? eventDate, bool homeTeamScore)
        {
            EventType = eventType;
            EventDate = eventDate;
            HomeTeamScore = homeTeamScore;
        }

        public required EventType EventType { get; set; }
        public DateTime? EventDate { get; set; }
        public bool HomeTeamScore { get; set; }
    }
}
