﻿using System.ComponentModel;

namespace TDDSportradarScoreboard.Model.Enum
{
    public enum Nationality
    {
        [Description("Uruguay")]
        Uruguay,

        [Description("Italy")]
        Italy,

        [Description("Spain")]
        Spain,

        [Description("Brazil")]
        Brazil,

        [Description("Mexico")]
        Mexico,

        [Description("Canada")]
        Canada,

        [Description("Argentina")]
        Argentina,

        [Description("Australia")]
        Australia,

        [Description("Germany")]
        Germany,

        [Description("France")]
        France,

        [Description("Serbia")]
        Serbia,

        [Description("Slovenia")]
        Slovenia,

        [Description("Japan")]
        Japan,

        [Description("Croatia")]
        Croatia,

        [Description("England")]
        England,

        [Description("Hungary")]
        Hungary,

        [Description("Austria")]
        Austria,

        [Description("China")]
        China
    }
}