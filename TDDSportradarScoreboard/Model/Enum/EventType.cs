﻿using System.ComponentModel;

namespace TDDSportradarScoreboard.Model.Enum
{
    public enum EventType
    {
        [Description("Goal")]
        Goal,

        [Description("Forfeit")]
        Forfeit
    }
}
