﻿using System.ComponentModel;

namespace TDDSportradarScoreboard.Model.Enum
{
    public enum MatchStatus
    {
        [Description("Registered")]
        Registered,

        [Description("Started")]
        Started,

        [Description("Finished")]
        Finished,

        [Description("RegistrationFailed")]
        RegistrationFailed,
    }
}
