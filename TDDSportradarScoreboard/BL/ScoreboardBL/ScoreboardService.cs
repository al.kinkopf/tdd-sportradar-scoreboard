﻿using TDDSportradarScoreboard.Model;
using TDDSportradarScoreboard.Model.Enum;

namespace TDDSportradarScoreboard.BL.ScoreboardBL
{
    public class ScoreboardService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="scoreBoard"></param>
        /// <param name="match"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public static void UpdateScoreBoard(ScoreBoard scoreBoard, Match match)
        {
            if (scoreBoard == null)
            {
                throw new ArgumentNullException();
            }
            else if (scoreBoard.MatchList == null)
            {
                scoreBoard.MatchList = [match];
            }
            else
            {
                scoreBoard.MatchList.Add(match);
            }
        }


        /// <summary>
        /// Ordered finished and currently playing matches first by sum of goals and then by start date
        /// </summary>
        /// <param name="scoreBoard"></param>
        /// <returns></returns>
        public static  ScoreBoard GetScoreBoardOrderedAll(ScoreBoard scoreBoard)
        {
            ScoreBoard result = new();

            if (IsScoreBoardValid(scoreBoard))
            {
                var orderedMatchList = scoreBoard.MatchList
                    .OrderByDescending(x => x.HomeTeamScore + x.AwayTeamScore)
                    .ThenByDescending(x => x.StartDate)
                    .ThenByDescending(x => x.Id).ToList();

                result.MatchList = (List<Match>?)orderedMatchList;
            }
            return result;
        }



        /// <summary>
        /// Ordered finished matches first by sum of goals and then by start date
        /// </summary>
        /// <param name="scoreBoard"></param>
        /// <returns></returns>
        public static ScoreBoard GetScoreBoardOrderedByStatus(ScoreBoard scoreBoard, MatchStatus status)
        {
            ScoreBoard result = new();

            if (IsScoreBoardValid(scoreBoard))
            {
                var orderedMatchList = scoreBoard.MatchList
                    .Where(x => x.MatchStatus == status)
                    .OrderByDescending(x => x.HomeTeamScore + x.AwayTeamScore)
                    .ThenByDescending(x => x.StartDate)
                    .ThenByDescending(x => x.Id).ToList();

                result.MatchList = (List<Match>?)orderedMatchList;
            }
            return result;
        }


        public static bool IsScoreBoardValid(ScoreBoard scoreBoard)
        {
            if (scoreBoard == null || scoreBoard.MatchList == null || scoreBoard.MatchList.Count() < 1)
            {
                return false;
            }
            else return true;
        }
    }
}
