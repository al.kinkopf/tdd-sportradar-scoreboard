﻿using TDDSportradarScoreboard.Model.Enum;
using TDDSportradarScoreboard.Model;

namespace TDDSportradarScoreboard.BL.EventBL
{
    public class EventService
    {
        /// <summary>
        /// Create an event and update the score
        /// </summary>
        /// <param name="match"></param>
        /// <param name="eventInput"></param>
        public static bool CheckAndCreateEvent(Match match, CreateEventInput eventInput)
        {
            if (IsEventValid(match, eventInput.EventType))
            {
                var eventevent = new Event(eventInput.EventType, eventInput.EventDate);

                //Update score/match regarding the event
                switch (eventInput.EventType)
                {
                    case EventType.Goal:
                        if (eventInput.HomeTeamScore)
                        {
                            match.HomeTeamScore++;
                            match.EventList.Add(eventevent);
                            break;
                        }
                        else
                        {
                            match.AwayTeamScore++;
                            match.EventList.Add(eventevent);
                            break;
                        }
                    default:
                        //The match situation and type of event is not yet implemented
                        return false;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  Check if business rules are valid for creating the event - only started match can have an event
        /// </summary>
        /// <param name="match"></param>
        /// <param name="eventType"></param>
        /// <returns></returns>
        public static bool IsEventValid(Match match, EventType eventType)
        {
            //Cannot score a goal if match is not started
            if (match.MatchStatus != MatchStatus.Started && eventType == EventType.Goal)
            {
                return false;
            }

            else return true;
        }
    }
}
