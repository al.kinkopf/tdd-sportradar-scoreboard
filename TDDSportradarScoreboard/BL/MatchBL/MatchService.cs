﻿using TDDSportradarScoreboard.BL.ScoreboardBL;
using TDDSportradarScoreboard.Model.Enum;
using TDDSportradarScoreboard.Model;

namespace TDDSportradarScoreboard.BL.MatchBL
{
    public class MatchService
    {
        /// <summary>
        /// Method for starting match.
        /// Match startdate and status should change. Scoreboard should be update with match.
        /// </summary>
        /// <param name="match"></param>
        /// <param name="startDate"></param>
        public static bool StartMatch(Match match, ScoreBoard scoreBoard, DateTime? startDate = null)
        {
            if (IsMatchStartValid(match, scoreBoard))
            {
                match.MatchStatus = MatchStatus.Started;

                if (startDate == null)
                {
                    match.StartDate = DateTime.Now;
                }
                else
                {
                    match.StartDate = (DateTime)startDate;
                }
                ScoreboardService.UpdateScoreBoard(scoreBoard, match);
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Method for finishing match.
        /// Match enddate and status should be updated
        /// </summary>
        /// <param name="match"></param>
        /// <param name="endDate"></param>
        public static bool FinishMatch(Match match, DateTime? endDate = null)
        {
            if (match.MatchStatus == MatchStatus.Started)
            {
                match.MatchStatus = MatchStatus.Finished;

                if (endDate == null)
                {
                    match.EndDate = DateTime.Now;
                    return true;
                }
                else
                {
                    match.EndDate = (DateTime)endDate;
                    return true;
                }
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Check if business rules are valid for starting the match
        /// Business rule:
        /// 1. Only registered match can start
        /// 2. only matches with teams not playing at the moment can start
        /// </summary>
        /// <param name="match"></param>
        /// <param name="scoreBoard"></param>
        /// <returns></returns>
        public static bool IsMatchStartValid(Match match, ScoreBoard scoreBoard)
        {
            if (match.MatchStatus == MatchStatus.Registered)
            {
                //check if scoreboard has already a started not finished match with a team that wants to start now
                if (scoreBoard.MatchList != null && scoreBoard.MatchList.Count > 0)
                {
                    var matchInProgress = scoreBoard.MatchList.Where(x => x.MatchStatus == MatchStatus.Started).ToList();
                    if (matchInProgress.Any())
                    {
                        if (matchInProgress.Where(x => x.HomeTeam.Nationality == match.HomeTeam.Nationality || x.HomeTeam.Nationality == match.AwayTeam.Nationality).ToList().Count() > 0)
                        {
                            return false;
                        }
                        else if (matchInProgress.Where(x => x.AwayTeam.Nationality == match.HomeTeam.Nationality || x.AwayTeam.Nationality == match.AwayTeam.Nationality).ToList().Count() > 0)
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            else { return false; }
        }
    }
}
